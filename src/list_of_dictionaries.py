if __name__ == '__main__':
    addresses_list = [
        {"city": "New York", "street": "Main Street", "house_number": "24", "post_code": "255667"},
        {"city": "London", "street": "Rose Lane", "house_number": "5", "post_code": "267667"},
        {"city": "Berlin", "street": "Karls Strasse", "house_number": "78", "post_code": "766776"}
    ]

    print(addresses_list[-1]["post_code"])
    print(addresses_list[1]["city"])
    addresses_list[0]["street"] = "54 Lane"
    print(addresses_list)
