def check_if_normal_conditions(temperature_in_celcius_degrees, air_pressure):
    if temperature_in_celcius_degrees == 0 and air_pressure == 1013:
        return True
    else:
        return False


def is_it_japanese_car(car_brand):
    if car_brand in ["Toyota", "Suzuki", "Mazda"]:
        return True
    else:
        return False


if __name__ == '__main__':
    print(check_if_normal_conditions(0, 1013))
    print(check_if_normal_conditions(1, 1013))
    print(check_if_normal_conditions(0, 1014))
    print(check_if_normal_conditions(1, 1014))

    print("Is the car Japanese?: ", is_it_japanese_car("KIA"))
    print("Is the car Japanese?: ", is_it_japanese_car("Mazda"))
