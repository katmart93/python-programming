import uuid


def print_first_ten_integers_squared():
    for integer in range(1, 31):
        print(integer ** 2)


def print_10_random_uuids(num_of_strings):
    for i in range(num_of_strings):
        print(str(uuid.uuid4()))


def print_nums_divisible_by_7(from_num, to_num):
    for number in range(from_num, to_num + 1):
        if number % 7 == 0:
            print(number)


def print_temperatures_in_both_scales(list_of_temps_in_celsius):
    for temp in list_of_temps_in_celsius:
        temp_fahrenheit = temp * 9 / 5 + 32
        print(f"Celsius: {temp}, Fahrenheit: {temp_fahrenheit}")


def get_temperatures_higher_than_20_degrees(list_of_temps_in_celsius):
    filtered_temps = []
    for temp in list_of_temps_in_celsius:
        if temp > 20:
            filtered_temps.append(temp)
    return filtered_temps


def convert_celsius_to_fahrenheit(list_of_temps_in_celsius):
    temps_in_fahrenheit = []
    for temp in list_of_temps_in_celsius:
        temps_in_fahrenheit.append(temp * 9 / 5 + 32)
    return temps_in_fahrenheit


if __name__ == '__main__':
    # print_10_random_uuids(10)
    # print_nums_divisible_by_7(0, 30)
    # print_temperatures_in_both_scales([10.3, 23.4, 15.8, 19.0, 14.0, 23.0, 25.0])
    # print(get_temperatures_higher_than_20_degrees([15, 10, 20, 35, 11, 24, 16, 21]))
    # print(convert_celsius_to_fahrenheit([10.3, 23.4, 15.8, 19.0, 14.0, 23.0, 25.0]))
    print_first_ten_integers_squared()
