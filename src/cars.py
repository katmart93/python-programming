def get_country_of_a_car_brand(car_brand):
    car_brand_lowercase = car_brand.lower()
    if car_brand_lowercase in ("toyota", "mazda", "suzuki", "subaru"):
        return "Japan"
    elif car_brand_lowercase in ("bmw", "mercedes", "audi", "volkswagen"):
        return "Germany"
    elif car_brand_lowercase in ("renault", "peugeot"):
        return "France"
    else:
        return "Unknown"


def test_get_country_for_a_japanese_car_capitalized():
    assert get_country_of_a_car_brand("Toyota") == "Japan"


def test_get_country_for_a_japanese_car_lowercase():
    assert get_country_of_a_car_brand("toyota") == "Japan"


def test_get_country_for_a_japanese_car_uppercase():
    assert get_country_of_a_car_brand("TOYOTA") == "Japan"


def test_get_country_for_a_german_car_capitalized():
    assert get_country_of_a_car_brand("Mercedes") == "Germany"


def test_get_country_for_a_german_car_lowercase():
    assert get_country_of_a_car_brand("mercedes") == "Germany"


def test_get_country_for_a_german_car_uppercase():
    assert get_country_of_a_car_brand("MERCEDES") == "Germany"


def test_get_country_for_a_french_car_capitalized():
    assert get_country_of_a_car_brand("Peugeot") == "France"


def test_get_country_for_a_french_car_lowercase():
    assert get_country_of_a_car_brand("peugeot") == "France"


def test_get_country_for_a_french_car_uppercase():
    assert get_country_of_a_car_brand("PEUGEOT") == "France"


def test_get_country_for_a_italian_car():
    assert get_country_of_a_car_brand("Fiat") == "Unknown"
