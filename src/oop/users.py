class User:
    def __init__(self, email, gender):
        self.email = email
        self.gender = gender
        self.usage_time = 0

    def use_application(self, usage_time_in_seconds):
        self.usage_time += usage_time_in_seconds


if __name__ == '__main__':
    user_kate = User('kate@example.com', 'female')
    user_james = User('james@examle.com', 'male')
    print(user_kate.email)
    print(user_james.email)
    user_kate.email = 'kate.new@example.com'
    print(user_kate.email)
    print(user_kate.usage_time)
    print(user_james.usage_time)
    user_kate.use_application(100)
    print(user_kate.usage_time)
    print(user_james.usage_time)
