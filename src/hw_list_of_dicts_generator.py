import random

# Listy zawierające dane do losowania

# Przykład jak można losować imię z listy
# random_female_firstname = random.choice(female_fnames)
# print(random_female_firstname)

# Przykład jak można losować wiek z liczb całkowitych od 1 do 65
# random_age = random.randint(1, 65)
# print(random_age)

# Przykładowy wygenerowany pojedynczy słownik
example_dictionary = {
    'firstname': 'Kate',
    'lastname': 'Yu',
    'email': 'kate.yu@example.com',
    'age': 23,
    'country': 'Poland',
    'adult': True
}


female_fnames = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka']
male_fnames = ['James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
surnames = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
countries = ['Poland', 'United Kingdom', 'Germany', 'France', 'Other']


def generate_personal_data_dict(firstname):
    rand_surname = random.choice(surnames)
    rand_country = random.choice(countries)
    rand_age = random.randint(5, 46)
    generated_dict = {
        "firstname": firstname,
        "lastname": rand_surname,
        "email": f"{firstname.lower()}.{rand_surname.lower()}@example.com",
        "age": rand_age,
        "country": rand_country,
        "adult": True if rand_age >= 18 else False,
        "birth_year": 2023 - rand_age
    }
    return generated_dict


def generate_list_of_personal_data_dicts():
    list_of_pers_data_dicts = []
    i = 0
    while i < 5:
        rand_fem_name = random.choice(female_fnames)
        list_of_pers_data_dicts.append(generate_personal_data_dict(rand_fem_name))
        i += 1
    i = 0
    while i < 5:
        rand_m_name = random.choice(male_fnames)
        list_of_pers_data_dicts.append(generate_personal_data_dict(rand_m_name))
        i += 1
    for person in list_of_pers_data_dicts:
        print(f"Hi! I'm {person['firstname']} {person['lastname']}. I come from {person['country']} and I was born in "
              f"{person['birth_year']}.")
    return list_of_pers_data_dicts


if __name__ == '__main__':
    # print(generate_personal_data_dict(rand_fem_name))
    # print(generate_personal_data_dict(rand_m_name))
    print(generate_list_of_personal_data_dicts())
